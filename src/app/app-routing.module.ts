import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { CampaignItemComponent } from './campaign/campaign-item/campaign-item.component';
import { CampaignComponent } from './campaign/campaign.component';
import { SponsorCampaignComponent } from './campaign/sponsor-campaign/sponsor-campaign.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'campaigns', component: CampaignComponent},
  {path: 'campaigns/:categoryId', component: CampaignItemComponent},
  {path: 'capaign-sponsor/:campaignId', component: SponsorCampaignComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'contact', component: ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
