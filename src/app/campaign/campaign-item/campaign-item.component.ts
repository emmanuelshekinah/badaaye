import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-campaign-item',
  templateUrl: './campaign-item.component.html',
  styleUrls: ['./campaign-item.component.scss']
})
export class CampaignItemComponent implements OnInit {

  category = "Maintanance Essentials"
  campaigns = [
    {
      id: 1, 
      title: "Nka thuto primary school", 
      address: "Soweto - Gauteng", 
      decription: "18 broken toilets in all washrooms",
      image: "assets/img/services-1.jpg",
      amount: 20000,
      status: 1
    },
    {
      id: 2, 
      title: "Nka thuto primary school", 
      address: "Soweto - Gauteng", 
      decription: "18 broken toilets in all washrooms",
      image: "assets/img/services-1.jpg",
      amount: 20000,
      status: 0
    },
    {
      id: 3, 
      title: "Nka thuto primary school", 
      address: "Soweto - Gauteng", 
      decription: "18 broken toilets in all washrooms",
      image: "assets/img/services-1.jpg",
      amount: 20000,
      status: 1
    },
    
  ] 
  constructor(private route:ActivatedRoute) { 
    
  }
    
  ngOnInit(): void {
    let id = this.route.snapshot.params['categoryId'];
    
  }

}
