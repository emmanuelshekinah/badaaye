import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SponsorCampaignComponent } from './sponsor-campaign.component';

describe('SponsorCampaignComponent', () => {
  let component: SponsorCampaignComponent;
  let fixture: ComponentFixture<SponsorCampaignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SponsorCampaignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SponsorCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
