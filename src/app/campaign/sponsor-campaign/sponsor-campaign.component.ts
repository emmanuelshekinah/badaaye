import { Component, OnInit } from '@angular/core';
import { Options} from '@angular-slider/ngx-slider';

@Component({
  selector: 'app-sponsor-campaign',
  templateUrl: './sponsor-campaign.component.html',
  styleUrls: ['./sponsor-campaign.component.scss']
})
export class SponsorCampaignComponent implements OnInit {

  constructor() { }
  title = 'Nka thuto primary school';  
  value: number = 0;  
  options: Options = {  
      floor: 0,  
      ceil: 20000  
  };  
  category  = "Maintanance Essesntials"
  campName = "Nka thuto primary school"
  image     = "assets/img/services-1.jpg"
  data = {
    id: 1, 
    title: "Nka thuto primary school", 
    address: "Soweto - Gauteng", 
    decription: "18 broken toilets in all washrooms",
    image: "assets/img/services-1.jpg",
    amount: 20000,
    fundRaised: 20000,
    fundeLeft: 14000,
    status: 1,
    startDate: "01 March 2022",
    endDate: "01 March 2022",
  }
  progress=67

  sliderValue = 0;
  sliderEvent() {
    
  }
  ngOnInit(): void {
  }

}
