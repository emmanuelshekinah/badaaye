import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-campaign-categories',
  templateUrl: './campaign-categories.component.html',
  styleUrls: ['./campaign-categories.component.scss']
})
export class CampaignCategoriesComponent implements OnInit {

  @Input() title!: string;
  @Input() id!: any;

  constructor() { }

  ngOnInit(): void {
  }

}
