import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {

  compaignData= [
    {id: 1, title: 'Campaign 1'},
    {id: 2, title: 'Campaign 2'},
    {id: 3, title: 'Campaign 3'},
  ];

  constructor() { }

  ngOnInit(): void {
    

  }

}
